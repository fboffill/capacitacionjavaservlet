<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Formulario</title>
    </head>
    <body>
        <h1>Formulario de la clase 2</h1>
        <form action="CapturaFormulario" method="GET">
            <p>Nombre: <input type="text" name="nombre" required="" /></p>
            <p>Apellido: <input type="text" name="apellido" required /></p>
            <p>Edad: <input type="number" name="edad" required /></p>
            <p>Experiencia: 
            <select name="experiencia">
                <option value="0">Seleccione...</option>
                <option value="Programador">Programador</option>
                <option value="Diseño">Diseño</option>
                <option value="Lider de Proyecto">Lider de Proyecto</option>
            </select></p>
            <hr />
            <input type="submit" value="Enviar Formulario" />
        </form>
    </body>
</html>
