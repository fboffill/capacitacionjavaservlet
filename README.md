Bitbucket Clase Java Servlet - Maven
=============

Este proyecto, comienza implementando un servlet simple, que permite la captura de información de un formulario. 
La comunicación del formulario con el servlet, puede ser via get o post.

También se utilizaron las anotaciones para acceder al servlet. Permitiendo de esta forma acceder al servlet, mediante URL/CapturaFormulario 
@WebServlet(name = "CapturaFormulario", urlPatterns = {"/CapturaFormulario"})

Please note that **only the first step** is covered by this gem — the rest happens on GitHub.com.  In particular, `markup` itself does no sanitization of the resulting HTML, as it expects that to be covered by whatever pipeline is consuming the HTML.

Please see our [contributing guidelines](CONTRIBUTING.md) before reporting an issue.

Installation
-----------
Es un proyecto muy simple y ejecuta en Tomcat 8/java 8

Uso
-----
Ejecutar el proyecto sobre el servidor de aplicaciones, y acceder a la siguiente URL http://localhost:8080/Clase2-Ejercicio2/CapturaFormulario
Esto mostrará el acceso al servlet de la siguiente manera: 

Datos capturados por el método doGet
El nombre ingresado es:null

El apellido ingresado es:null

La edad ingresada es:null

Y la experiencia ingresada es:null

En cambio, si lo accedemos desde el formjulario creado para la captura de información.
http://localhost:8080/Clase2-Ejercicio2/formulario.jsp
Cargamos la información solicitada y presionamos el boton "envíar". El servlet, retorna la información ingresada.

Datos capturados por el método doGet
El nombre ingresado es:Fernando

El apellido ingresado es:Boffill

La edad ingresada es:23

Y la experiencia ingresada es:Programador



