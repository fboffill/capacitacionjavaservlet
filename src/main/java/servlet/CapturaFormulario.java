/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author cudve
 */
@WebServlet(name = "CapturaFormulario", urlPatterns = {"/CapturaFormulario"})
public class CapturaFormulario extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String edad = req.getParameter("edad");
        String experiencia = req.getParameter("experiencia");
        
        PrintWriter out = resp.getWriter();
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Mi Formulario </title>");
        out.println("</head>");
        out.println("<body>");
        out.print("<h1>Datos capturados por el método doGet</h1>");
        out.print("<p>El nombre ingresado es:" + nombre + "</p>");
        out.print("<p>El apellido ingresado es:" + apellido + "</p>");
        out.print("<p>La edad ingresada es:" + edad + "</p>");
        out.print("<p>Y la experiencia ingresada es:" + experiencia + "</p>");
        out.println("</body>");
        out.println("</html>");
        
        //http://localhost:8080/Clase2-Ejercicio2/CapturaFormulario?nombre=Roberto&apellido=Garcia&edad=44&experiencia=Programador
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String edad = req.getParameter("edad");
        String experiencia = req.getParameter("experiencia");
        
        PrintWriter out = resp.getWriter();
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Mi Formulario </title>");
        out.println("</head>");
        out.println("<body>");
        out.print("<h1>Datos capturados por el método doPost</h1>");
        out.print("<p>El nombre ingresado es:" + nombre + "</p>");
        out.print("<p>El apellido ingresado es:" + apellido + "</p>");
        out.print("<p>La edad ingresada es:" + edad + "</p>");
        out.print("<p>Y la experiencia ingresada es:" + experiencia + "</p>");
        out.println("</body>");
        out.println("</html>");
    }
}
